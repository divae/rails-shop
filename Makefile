build:
	docker-compose build

up:
	docker-compose up

test:
	docker-compose run app rspec

run:
	docker-compose run app
