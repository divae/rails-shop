db.createUser(
  {
    user: "mongoroot",
    pwd: "mongosecret",
    roles: [
      {
        role: "readWrite",
        db: "db_development"
      }
    ]
  }
)
